﻿using System;
using System.Collections.Generic;

namespace Model.Param.SystemManage
{
    public class AutoJobLogListParam
    {
        public string JobName { get; set; }
    }
}
