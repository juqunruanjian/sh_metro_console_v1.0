﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Util;

namespace Model.Param.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-17 16:19
    /// 描 述：员工信息实体查询类
    /// </summary>
    public class EmployeeListParam
    {
    }
    public class EmployeePartTimeListVM
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long Id { get; set; }
        public string DeptName { get; set; }
        public string JobName { get; set; }
        public string PartType { get; set; }
    }
}
