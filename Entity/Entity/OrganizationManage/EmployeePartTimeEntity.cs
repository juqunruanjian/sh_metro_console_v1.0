﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace Entity.OrganizationManage
{
    [Table("SysEmployeePartTime")]
    public class EmployeePartTimeEntity : BaseEntity
    {
        public long EmpId { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        public long DeptId { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        public long JobId { get; set; }
        public int Type { get; set; }
    }
}
