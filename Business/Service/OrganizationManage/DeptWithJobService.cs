﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;
using Util.Extension;
using Util.Model;
using Data;
using Data.Repository;
using Entity.OrganizationManage;
using Model.Param.OrganizationManage;
using IdGenerator;

namespace Service.OrganizationManage
{
    public class DeptWithJobService: RepositoryFactory
    {
        public async Task SaveDeptWithJob(DeptWithJobEntity entity)
        {
            long[] dept_idArr = TextHelper.SplitToArray<long>(entity.DeptIds, ',');
            long[] job_idArr = TextHelper.SplitToArray<long>(entity.JobIds, ',');
            var listEntity = new List<DeptWithJobEntity>();
            foreach (var dept in dept_idArr)
            {
                foreach (var job in job_idArr)
                {
                    listEntity.Add(new DeptWithJobEntity() 
                    {
                        DeptId=dept,
                        JobId=job,
                        Id = IdGeneratorHelper.Instance.GetId()
                });
                    
                }
            }
            await this.BaseRepository().Insert(listEntity);
        }

    }
}
