﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Data.Repository;
using Entity.OrganizationManage;
using Entity.SystemManage;
using Enum.SystemManage;
using Model;
using Model.Param.OrganizationManage;
using Util;
using Util.Extension;
using Util.Model;

namespace Service.OrganizationManage
{
    public class DepartmentService : RepositoryFactory
    {
        #region 获取数据
        public async Task<List<DepartmentEntity>> GetList(DepartmentListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.OrderBy(p => p.DepartmentSort).ToList();
        }

        public async Task<DepartmentEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<DepartmentEntity>(id);
        }
        public async Task<DepartmentEntity> GetEntity(string name)
        {
            return await this.BaseRepository().FindEntity<DepartmentEntity>(p => p.DepartmentName == name);
        }
        public async Task<int> GetMaxSort()
        {
            object result = await this.BaseRepository().FindObject("SELECT MAX(DepartmentSort) FROM SysDepartment");
            int sort = result.ParseToInt();
            sort++;
            return sort;
        }
        public async Task<List<long>> GetJobIds(long deptId) 
        {
            var ids = new List<long>();
            var listIds = await this.BaseRepository().FindList<DeptWithJobEntity>("Select JobId From SysDeptWithJob Where DeptId="+deptId);
            foreach (var item in listIds.ToList())
            {
                ids.Add(item.JobId);
            }
            return ids;
        }
        /// <summary>
        /// 部门名称是否存在
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool ExistDepartmentName(DepartmentEntity entity)
        {
            var expression = LinqExtensions.True<DepartmentEntity>();
            expression = expression.And(t => t.BaseIsDelete == 0);
            if (entity.Id.IsNullOrZero())
            {
                expression = expression.And(t => t.DepartmentName == entity.DepartmentName);
            }
            else
            {
                expression = expression.And(t => t.DepartmentName == entity.DepartmentName && t.Id != entity.Id);
            }
            return this.BaseRepository().IQueryable(expression).Count() > 0 ? true : false;
        }

        /// <summary>
        /// 是否存在子部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ExistChildrenDepartment(long id)
        {
            var expression = LinqExtensions.True<DepartmentEntity>();
            expression = expression.And(t => t.ParentId == id);
            return this.BaseRepository().IQueryable(expression).Count() > 0 ? true : false;
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(DepartmentEntity entity)
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                if (entity.Id.IsNullOrZero())
                {
                    await entity.Create();
                    await db.Insert(entity);
                }
                else
                {
                    await db.Delete<MenuAuthorizeEntity>(t => t.AuthorizeId == entity.Id);
                    await entity.Modify();
                    await db.Update(entity);
                }
                // 角色对应的菜单、页面和按钮权限
                if (!string.IsNullOrEmpty(entity.MenuIds))
                {
                    foreach (long menuId in TextHelper.SplitToArray<long>(entity.MenuIds, ','))
                    {
                        MenuAuthorizeEntity menuAuthorizeEntity = new MenuAuthorizeEntity();
                        menuAuthorizeEntity.AuthorizeId = entity.Id;
                        menuAuthorizeEntity.MenuId = menuId;
                        menuAuthorizeEntity.AuthorizeType = AuthorizeTypeEnum.Dept.ParseToInt();
                        await menuAuthorizeEntity.Create();
                        await db.Insert(menuAuthorizeEntity);
                    }
                }
                await db.CommitTrans();
            }
            catch
            {
                await db.RollbackTrans();
                throw;
            }
        }

        public async Task DeleteForm(string ids)
        {
            var db = await this.BaseRepository().BeginTrans();
            try
            {
                long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
                await db.Delete<DepartmentEntity>(idArr);
                await db.CommitTrans();
            }
            catch
            {
                await db.RollbackTrans();
                throw;
            }
        }
        #endregion

        #region 私有方法
        private Expression<Func<DepartmentEntity, bool>> ListFilter(DepartmentListParam param)
        {
            var expression = LinqExtensions.True<DepartmentEntity>();
            if (param != null)
            {
                if (!param.DepartmentName.IsEmpty())
                {
                    expression = expression.And(t => t.DepartmentName.Contains(param.DepartmentName));
                }
                if (!param.DeptType.IsNullOrZero()) 
                {
                    expression = expression.And(t => t.DeptType == param.DeptType);
                }
            }
            return expression;
        }
        #endregion
    }
}
