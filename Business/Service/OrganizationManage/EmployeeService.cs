﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;
using Util.Extension;
using Util.Model;
using Data;
using Data.Repository;
using Entity.OrganizationManage;
using Model.Param.OrganizationManage;

namespace Service.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-17 16:19
    /// 描 述：员工信息服务类
    /// </summary>
    public class EmployeeService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<EmployeeEntity>> GetList(EmployeeListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.ToList();
        }

        public async Task<List<EmployeeEntity>> GetPageList(EmployeeListParam param, Pagination pagination)
        {
            var expression = ListFilter(param);
            var list= await this.BaseRepository().FindList(expression, pagination);
            return list.ToList();
        }

        public async Task<EmployeeEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<EmployeeEntity>(id);
        }
        public async Task<EmployeeEntity> GetEntity(string idNumber)
        {
            return await this.BaseRepository().FindEntity<EmployeeEntity>(p => p.EmpWorkID == idNumber);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(EmployeeEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<EmployeeEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private Expression<Func<EmployeeEntity, bool>> ListFilter(EmployeeListParam param)
        {
            var expression = LinqExtensions.True<EmployeeEntity>();
            if (param != null)
            {
            }
            return expression;
        }
        #endregion
    }
}
