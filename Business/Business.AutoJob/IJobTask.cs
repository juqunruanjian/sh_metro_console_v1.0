﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Util.Model;

namespace Business.AutoJob
{
    public interface IJobTask
    {
        Task<TData> Start();
    }
}
