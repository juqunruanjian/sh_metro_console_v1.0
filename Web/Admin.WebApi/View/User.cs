﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Admin.WebApi
{
    public class UserModel
    {
        public long userId { get; set; }
        public string userName { get; set; }
        public List<EmployeeModel> empList { get; set; }
        public List<EmployeePartTimeModel> empPartTimeList { get; set; }
    }
    public class EmployeeModel
    {
        public string realName { get; set; }
        public string workId { get; set; }
        public int empState { get; set; }
        public long empId { get; set; }
    }
    public class EmployeePartTimeModel
    { 
        public long Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
    }
    public class RolesModel 
    {
        
    }
}
