﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Web.Code
{
    public class OperatorInfo
    {
        public long? UserId { get; set; }
        public int? UserStatus { get; set; }
        public int? IsOnline { get; set; }
        public string UserName { get; set; }
        public string RealName { get; set; }
        public string WebToken { get; set; }
        public string ApiToken { get; set; }
        public int? IsSystem { get; set; }
        public string Portrait { get; set; }
        public long? EmpId { get; set; }
        /// <summary>
        /// 角色Id
        /// </summary>
        [NotMapped]
        public List<RoleInfo> RoleInfo { get; set; }
        [NotMapped]
        public List<DepInfo> DeptInfo { get; set; }
        [NotMapped]
        public List<JobInfo> JobInfo { get; set; }
    }
    public class RoleInfo
    {
        public long RoleId { get; set; }
        public string RoleName { get; set; }
    }
    public class DepInfo 
    {
        public string DeptName { get; set; }
        public long DeptId { get; set; }
        public int PartTimeType { get; set; }
    }
    public class JobInfo
    {
        public string JobName { get; set; }
        public long JobId { get; set; }
        public int PartTimeType { get; set; }
    }
}
