﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Admin.Web.Controllers;
using Business.OrganizationManage;
using Entity.OrganizationManage;
using Model;
using Model.Param.OrganizationManage;
using Model.Result;
using Util.Model;
namespace Admin.Web.Areas.OrganizationManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2021-05-18 14:26
    /// 描 述：岗位信息控制器类
    /// </summary>
    [Area("OrganizationManage")]
    public class JobController :  BaseController
    {
        private JobBLL jobBLL = new JobBLL();
        private DepartmentBLL departmentBLL = new DepartmentBLL();
        #region 视图功能
        [AuthorizeFilter("organization:job:view")]
        public IActionResult jobIndex()
        {
            return View();
        }
        public IActionResult jobForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("organization:job:search,organization:user:search")]
        public async Task<IActionResult> GetListJson(JobListParam param)
        {
            TData<List<JobEntity>> obj = await jobBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("organization:job:search,organization:user:search")]
        public async Task<IActionResult> GetJobTreeListJson()
        {
            TData<List<ZtreeInfo>> obj = await jobBLL.GetZtreeJobList(0);
            return Json(obj);
        }
        [HttpGet]
        [AuthorizeFilter("organization:department:search,organization:user:search")]
        public async Task<IActionResult> GetDepartmentTreeListJson(DepartmentListParam param)
        {
            TData<List<ZtreeInfo>> obj = await departmentBLL.GetZtreeDepartmentList(param);
            return Json(obj);
        }
        [HttpGet]
        public async Task<IActionResult> GetUserTreeListJson(JobListParam param)
        {
            TData<List<ZtreeInfo>> obj = await jobBLL.GetZtreeUserList(param);
            return Json(obj);
        }

        [HttpGet]
        public async Task<IActionResult> GetFormJson(long id)
        {
            TData<JobEntity> obj = await jobBLL.GetEntity(id);
            return Json(obj);
        }

        [HttpGet]
        public async Task<IActionResult> GetMaxSortJson()
        {
            TData<int> obj = await jobBLL.GetMaxSort();
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("organization:job:add,organization:job:edit")]
        public async Task<IActionResult> SaveFormJson(JobEntity entity)
        {
            TData<string> obj = await jobBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("organization:job:delete")]
        public async Task<IActionResult> DeleteFormJson(string ids)
        {
            TData obj = await jobBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
