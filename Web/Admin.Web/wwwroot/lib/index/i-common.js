﻿function ChangeToTable(printDatagrid) {
    var tableString = '<table cellspacing="0" class="pb">';
    var frozenColumns = printDatagrid.datagrid("options").frozenColumns;  // 得到frozenColumns对象
    var columns = printDatagrid.datagrid("options").columns;    // 得到columns对象
    var nameList = new Array();

    // 载入title
    if (typeof columns != 'undefined' && columns != '') {
        $(columns).each(function (index) {
            tableString += '\n<tr>';
            if (typeof frozenColumns != 'undefined' && typeof frozenColumns[index] != 'undefined') {
                for (var i = 0; i < frozenColumns[index].length; ++i) {
                    if (!frozenColumns[index][i].hidden) {
                        tableString += '\n<th width="' + frozenColumns[index][i].width + '"';
                        if (typeof frozenColumns[index][i].rowspan != 'undefined' && frozenColumns[index][i].rowspan > 1) {
                            tableString += ' rowspan="' + frozenColumns[index][i].rowspan + '"';
                        }
                        if (typeof frozenColumns[index][i].colspan != 'undefined' && frozenColumns[index][i].colspan > 1) {
                            tableString += ' colspan="' + frozenColumns[index][i].colspan + '"';
                        }
                        if (typeof frozenColumns[index][i].field != 'undefined' && frozenColumns[index][i].field != '') {
                            nameList.push(frozenColumns[index][i]);
                        }
                        tableString += '>' + frozenColumns[0][i].title + '</th>';
                    }
                }
            }
            for (var i = 0; i < columns[index].length; ++i) {
                if (!columns[index][i].hidden) {
                    tableString += '\n<th width="' + columns[index][i].width + '"';
                    if (typeof columns[index][i].rowspan != 'undefined' && columns[index][i].rowspan > 1) {
                        tableString += ' rowspan="' + columns[index][i].rowspan + '"';
                    }
                    if (typeof columns[index][i].colspan != 'undefined' && columns[index][i].colspan > 1) {
                        tableString += ' colspan="' + columns[index][i].colspan + '"';
                    }
                    if (typeof columns[index][i].field != 'undefined' && columns[index][i].field != '') {
                        nameList.push(columns[index][i]);
                    }
                    tableString += '>' + columns[index][i].title + '</th>';
                }
            }
            tableString += '\n</tr>';
        });
    }
    // 载入内容
    var rows = printDatagrid.datagrid("getRows"); // 这段代码是获取当前页的所有行
    for (var i = 0; i < rows.length; ++i) {
        tableString += '\n<tr>';
        for (var j = 0; j < nameList.length; ++j) {
            var e = nameList[j].field.lastIndexOf('_0');

            tableString += '\n<td';
            if (nameList[j].align != 'undefined' && nameList[j].align != '') {
                tableString += ' style="text-align:' + nameList[j].align + ';vnd.ms-excel.numberformat:@"';
            }
            tableString += '>';
            if (e + 2 == nameList[j].field.length) {
                tableString += rows[i][nameList[j].field.substring(0, e)];
            }
            else
                tableString += rows[i][nameList[j].field];
            tableString += '</td>';
        }
        tableString += '\n</tr>';
    }
    tableString += '\n</table>';
    return tableString;
}


function openPageByLH(page, title, closable, param) {
    if (closable == null) closable = true;
    var temp = Math.random();
    var href = page + '?math=' + temp;
    //var href = page + '.aspx';
    if (param != null) href += '&' + param;
    if ($('#tt').tabs('exists', title)) {
        $('#tt').tabs('select', title);
        var tab = $('#tt').tabs('getSelected');  // 获取选择的面板
        var str = "<iframe src=\"" + href + "\" id=\"" + temp + "\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>";
        // $('#tt').tabs('select', title);
        $("#tt").tabs("update", {
            tab: tab,
            options: {
                content: str
            }
        });
    } else {
        var str = "<iframe src=\"" + href + "\" id=\"" + temp + "\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>";
        $('#tt').tabs('add', {
            title: title,
            content: str,
            closable: closable
        });
    }
}

function Export(strXlsName, exportGrid, exportPage) {
    var f = $('<form action="' + exportPage + '" method="post" id="fm1"></form>');
    var i = $('<input type="hidden" id="txtContent" name="txtContent" />');
    var l = $('<input type="hidden" id="txtName" name="txtName" />');
    i.val(ChangeToTable(exportGrid));
    i.appendTo(f);
    l.val(strXlsName);
    l.appendTo(f);
    f.appendTo(document.body).submit();
    document.body.removeChild(f);
}

//EasyUI datagrid 动态导出Excel
//function ExporterExcel(datagridId, excelName) {
//    //获取Datagride的列
//    var rows = $('#' + datagridId).datagrid('getRows');
//    var columns = $('#' + datagridId).datagrid("options").columns[0];
//    var oXL = new ActiveXObject("Excel.Application"); //创建AX对象excel
//    var oWB = oXL.Workbooks.Add(); //获取workbook对象
//    var oSheet = oWB.ActiveSheet; //激活当前sheet
//    //设置工作薄名称
//    oSheet.name = excelName;
//    //设置表头
//    for (var i = 0; i < columns.length; i++) {
//        oSheet.Cells(1, i+1).value = columns[i].title;
//    }
//    //设置内容部分
//    for (var i = 0; i < rows.length; i++) {
//        //动态获取每一行每一列的数据值
//        for (var j = 0; j < columns.length; j++) {              
//            oSheet.Cells(i + 2, j+1).value = rows[i][columns[j].field];
//        }  
//    }             
//    oXL.Visible = true; //设置excel可见属性
//}
// 验证
// 验证下拉列表
//$.extend($.fn.validatebox.defaults.rules, {
//    selectValueRequired: {
//        validator: function (value, param) {
//            return $(param[0]).combobox('getValue') != '';
//        },
//        message: 'select value required.'
//    }
//});

//格式化json日期
//dt 日期
//sfm true表示需要时分秒，false表示不需要时分秒
function formatDate(dt, sfm) {
    var year = dt.getFullYear();
    var month = dt.getMonth() + 1;
    var date = dt.getDate();
    var hour = dt.getHours();
    var minute = dt.getMinutes();
    var second = dt.getSeconds();
    return year + "-" + month + "-" + date + (sfm == "true" ? (" " + hour + ":" + minute + ":" + second) : "");
}

//格式化日历控件日期
//$.fn.datebox.defaults.formatter = function (date) {

//    var y = date.getFullYear();
//    var m = date.getMonth() + 1;
//    var d = date.getDate();
//    return y + '-' + m + '-' + d;
//}
// 日历添加清空按钮
//var buttons = $.extend([], $.fn.datebox.defaults.buttons);
//buttons.splice(1, 0, {
//    text: '清空',
//    handler: function (target) {
//        $("#" + target.id).datebox('setText', '');
//    }
//});

//newalert = function (msg) {
//    $.messager.alert('警告', msg);

//}
//window.alert = newalert;

function showDialogAndReloadDataGrid(reloadControl, divId, page, title, width, height, param) {
    if (title == null) title = 'Dialog';
    if (width == null) width = 500;
    if (height == null) height = 600;
    var temp = Math.random();
    var href = page + '?math=' + temp;
    if (param != null) href += '&' + param;
    var str = "<iframe src=\"" + href + "\" id=\"" + temp + "\" width=\"100%\" height=\"100%\" frameborder=\"0\" ></iframe>";
    $("#" + divId).html(str);
    $('#' + divId).dialog({
        title: title,
        width: width,
        height: height,
        closed: false,
        cache: false,
        resizable: true,
        modal: true,
        buttons: [{
            text: '关闭',
            iconCls: 'icon-cancel',
            handler: function () {
                $('#' + divId).dialog('close');
                $('#' + reloadControl).datagrid('reload');
            }
        }],
        onClose: function () {
            $('#' + reloadControl).datagrid('reload');
        }
    });
}

function openPageByLH(page, title, closable, param) {
    if (closable == null) closable = true;
    var temp = Math.random();
    var href = page + '?math=' + temp;
    //var href = page + '.aspx';
    if (param != null) href += '&' + param;
    if ($('#tt').tabs('exists', title)) {
        $('#tt').tabs('select', title);
        var tab = $('#tt').tabs('getSelected');  // 获取选择的面板
        var str = "<iframe src=\"" + href + "\" id=\"" + temp + "\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>";
        // $('#tt').tabs('select', title);
        $("#tt").tabs("update", {
            tab: tab,
            options: {
                content: str
            }
        });
    } else {
        var str = "<iframe src=\"" + href + "\" id=\"" + temp + "\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>";
        $('#tt').tabs('add', {
            title: title,
            content: str,
            closable: closable
        });
    }
}

function openPage(page, title, closable, param) {
    if (closable == null) closable = true;
    var temp = Math.random();
    var href = page + '?math=' + temp;
    //var href = page + '.aspx';
    if (param != null) href += '?' + param;
    if ($('#tt').tabs('exists', title)) {
        $('#tt').tabs('select', title);
    } else {
        var str = "<iframe src=\"" + href + "\" id=\"" + temp + "\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>";
        $('#tt').tabs('add', {
            title: title,
            content: str,
            closable: closable
        });
    }
}

function showDialog(divId, page, title, width, height, param) {
    if (title == null) title = 'Dialog';
    if (width == null) width = 500;
    if (height == null) height = 600;
    var temp = Math.random();
    var href = page + '?math=' + temp;
    if (param != null) href += '&' + param;
    var str = "<iframe src=\"" + href + "\" id=\"" + temp + "\" width=\"100%\" height=\"100%\" frameborder=\"0\" ></iframe>";
    $("#" + divId).html(str);
    $('#' + divId).dialog({
        title: title,
        width: width,
        height: height,
        closed: false,
        cache: false,
        resizable: true,
        modal: true,
        buttons: [{
            text: '关闭',
            iconCls: 'icon-cancel',
            handler: function () {
                $('#' + divId).dialog('close');
            }
        }]
    });
}


function myformatter(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return y + '-' + (m < 10 ? ('0' + m) : m) + '-' + (d < 10 ? ('0' + d) : d);
}
function myparser(s) {
    if (!s) return new Date();
    var ss = (s.split('-'));
    var y = parseInt(ss[0], 10);
    var m = parseInt(ss[1], 10);
    var d = parseInt(ss[2], 10);
    if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
        return new Date(y, m - 1, d);
    } else {
        return new Date();
    }
}

function ajaxErrInDatagrid(result, el) {
    result = result.responseText.substring(1);
    var err = jQuery.parseJSON(result);
    console.log(err.MsgAction);
    if (err.MsgAction != 2) {
        alert(err.ErrorMsg);
        if (err.MsgAction == 1) {
            var temp = window;
            while (temp.parent != temp) {
                temp = temp.parent;
            }
            temp.top.location = "/Login.aspx";
        }
    } else {
        console.log(err);
        console.log(err.JsonResult);
        if (err.JsonResult.length > 0) {
            var temp = JSON2.parse(err.JsonResult);

            $(el).datagrid('loadData', temp);
        }
    }
}


function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
function AjaxCallBack(msg, SuccessCallBack) {
    var err = msg;
    if (err.MsgAction != 2) {
        alert(err.ErrorMsg);
        if (err.MsgAction == 1) {
            var temp = window;
            while (temp.parent != temp) {
                temp = temp.parent;
            }
            temp.top.location = "/Login.aspx";
            //if (window.parent) {
            //    window.parent.top.location = "/Login.aspx";
            //} else {
            //    window.top.location = "/Login.aspx";
            //}
        }
    } else {
        SuccessCallBack(err.JsonResult);
    }
}

function ClosePage(title) {
    if ($('#tt').tabs('exists', title)) {
        $("#tt").tabs("close", title);
    }
}

//返回日期年月日
function setDateToyyyyMMdd(date) {
    var result = "";
    var date = new Date(date);
    var year = date.getFullYear();
    var month = parseInt(date.getMonth() + 1) < 10 ? "0" + parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1);
    var dateTemp = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    result = year + "-" + month + "-" + dateTemp;
    return result;

}

//返回日期年月
function setDateToyyyyMM(date) {
    var result = "";
    var date = new Date(date);
    var year = date.getFullYear();
    var month = parseInt(date.getMonth() + 1) < 10 ? "0" + parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1);

    result = year + "-" + month
    return result;

}


//返回日期年月日时分秒
function setDateToyyyyMMhhHHmmss(date) {
    var result = "";
    var date = new Date(date);
    var year = date.getFullYear();
    var month = parseInt(date.getMonth() + 1) < 10 ? "0" + parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1);
    var dateTemp = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var minuts = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    result = year + "-" + month + "-" + dateTemp + " " + hour + ":" + minuts + ":" + seconds;
    return result;

}


//返回日期年月日时分秒 根据传入字符串判定  yyyy  yyyyMM yyyyMMdd yyyyMMddHHmmss
function getDateFormatter(date, type) {
    var result = "";
    var date = new Date(date);
    var year = date.getFullYear();
    var month = parseInt(date.getMonth() + 1) < 10 ? "0" + parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1);
    var dateTemp = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var minuts = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    switch (type) {
        case "yyyy":
            result = year;
            break;
        case "yyyyMM":
            result = year + "-" + month;
            break;
        case "yyyyMMdd":
            result = year + "-" + month + "-" + dateTemp;
            break;
        case "yyyyMMddHHmmss":
            result = year + "-" + month + "-" + dateTemp + " " + hour + ":" + minuts + ":" + seconds;
            break;
        case "MM":
            result = month
            break;
        case "dd":
            result = +dateTemp
            break;
        case "HHmmss":
            result = hour + ":" + minuts + ":" + seconds;
            break;
        case "mm":
            result = minuts
            break;
        case "ss":
            result = seconds;
            break;
        default:

    }

    return result;

}

//计算两个日期天数差的函数，通用 //sDate1和sDate2是yyyy-MM-dd格式
function DateDiff(sDate1, sDate2) {

    var date1 = parseInt(sDate1.replace(/-/g, ''), 10);
    var date2 = parseInt(sDate2.replace(/-/g, ''), 10);

    var iDays = date1 - date2;
    return iDays;  //返回相差天数

}
///获取当月最大天数
function getDaysInOneMonth(year, month) {
    month = parseInt(month, 10);
    var d = new Date(year, month, 0);
    return d.getDate();
}
//获取本月默认日期
function getNowMonthInitialize(day) {
    var res = "";
    var date = new Date();
    var year = date.getFullYear();
    var month = parseInt(date.getMonth() + 1) < 10 ? "0" + parseInt(date.getMonth() + 1) : parseInt(date.getMonth() + 1);
    var dateTemp = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var minuts = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

    if (date.getDate() < 21 && date.getMonth == 0) {//当小于1月21号     
        year = year - 1;
        month = 12;
    }
    else {
        if (parseInt(date.getDate()) < 21) {
            month = date.getMonth();
        }
    }

    res = year + "-" + month + "-" + day;
    return res;
}


function DateFormat(val) {
    if (val != null) {

        var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
        if (date.getFullYear() == 1900) {

            return "";
        }
        //月份为0-11，所以+1，月份小于10时补个0
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var result = "";
        var hour = "";
        if (date.getHours() > 0) {
            hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        }
        else {
            hour = date.getHours() == 0 ? "0" + date.getHours() : date.getHours();
        }
        var minuets = "";
        if (date.getMinutes() > 0) {
            minuets = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        }
        else {
            minuets = date.getMinutes() == 0 ? "0" + date.getMinutes() : date.getMinutes();
        }
        var seconds = "";
        if (date.getSeconds() > 0) {
            seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        }
        else {
            seconds = date.getSeconds() == 0 ? "0" + date.getSeconds() : date.getSeconds();
        }

        if (hour == "" && minuets == "" && seconds == "") {
            result = (date.getFullYear() + "-" + month + "-" + currentDate + " " + hour + minuets + seconds).toString();

        }
        else {
            result = (date.getFullYear() + "-" + month + "-" + currentDate + " " + hour + ":" + minuets + ":" + seconds).toString();
        }

        return result;
    }
    return "";
}

function DateFormatyyyyMMddHHmm(val) {
    if (val != null) {
        var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
        if (date.getFullYear() == 1900) {
            return "";
        }
        //月份为0-11，所以+1，月份小于10时补个0
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var result = "";
        var hour = "";
        if (date.getHours() > 0) {
            hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        }
        else {
            hour = date.getHours() == 0 ? "0" + date.getHours() : date.getHours();
        }
        var minuets = "";
        if (date.getMinutes() > 0) {
            minuets = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        }
        else {
            minuets = date.getMinutes() == 0 ? "0" + date.getMinutes() : date.getMinutes();
        }


        if (hour == "" && minuets == "" && seconds == "") {
            result = (date.getFullYear() + "-" + month + "-" + currentDate + " " + hour + minuets).toString();

        }
        else {
            result = (date.getFullYear() + "-" + month + "-" + currentDate + " " + hour + ":" + minuets).toString();
        }

        return result;
    }
    return "";
}


function DateFormatyyyyMMddGetJson(val) {
    if (val != null) {
        var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
        if (date.getFullYear() == 1900) {
            return "";
        }
        //月份为0-11，所以+1，月份小于10时补个0
        var year = date.getFullYear();
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var result = "";
        if (year != 1900) {
            result = (year + "-" + month + "-" + currentDate).toString();
        }

        return result;
    }
    return "";
}

function DateFormatyyyyMMGetJson(val) {
    if (val != null) {
        var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
        if (date.getFullYear() == 1900) {
            return "";
        }
        //月份为0-11，所以+1，月份小于10时补个0
        var year = date.getFullYear();
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var result = "";
        if (year != 1900) {
            result = (year + "-" + month).toString();
        }

        return result;
    }
    return "";
}



var Base64 =
{
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    },
    decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    },
    _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    },
    _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }
};



//获取浏览器页面可见高度和宽度  
var _PageHeight = document.documentElement.clientHeight,
    _PageWidth = document.documentElement.clientWidth;

//计算loading框距离顶部和左部的距离（loading框的宽度为215px，高度为61px）  
var _LoadingTop = _PageHeight > 61 ? (_PageHeight - 61) / 2 : 0,
    _LoadingLeft = _PageWidth > 215 ? (_PageWidth - 215) / 2 : 0;

//加载gif地址  
var Loadimagerul = "../css/loading.gif";

//在页面未加载完毕之前显示的loading Html自定义内容  
var _LoadingHtml = '<div id="loadingDiv" style="position:absolute;left:0;width:100%;height:' + _PageHeight + 'px;top:0;background:#f3f8ff;opacity:1;filter:alpha(opacity=80);z-index:10000;"><div style="position: absolute; cursor1: wait; left: ' + _LoadingLeft + 'px; top:' + _LoadingTop + 'px; width:100px;; height: 57px; line-height: 57px; padding-left: 50px; padding-right: 5px; background: #fff url(' + Loadimagerul + ') no-repeat scroll 10px 17px; border: 2px solid #95B8E7; color: #696969; font-family:\'Microsoft YaHei\';">加载中...</div></div>';

//呈现loading效果  
//document.write(_LoadingHtml);

//监听加载状态改变  
//document.onreadystatechange = completeLoading;

//加载状态为complete时移除loading效果  
function completeLoading() {
    if (document.readyState == "complete") {
        var loadingMask = document.getElementById('loadingDiv');
        loadingMask.parentNode.removeChild(loadingMask);
    }
}